
@extends('layout.adminmaster')

@section('title')

COM - Ultimate Freelance Marketplace
@endsection

@section('content')
 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
          <h3 class="content-header-title mb-0 d-inline-block">Ratings & Reviews</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item active">Ratings & Reviews
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="content-body">
        <!-- Basic form layout section start -->


        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header">
                  <h4 class="card-title">Reviews Management</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                </div>
              </div>

                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Sl.No</th>
                          <th>Job Name</th>
                          <th>Ratings</th>
                          <th>Review By</th>
                          <th>Comments</th>
                          <th>Created At</th>
                          <th>Updated At</th>
                          <th>Action</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>01</td>
                          <td>Job1</td>
                          <td>***</td>
                          <td>Client</td>
                          <td>This experience was wonderful</td>
                          <td>23/02/2018 15:01:35</td>                          
                          <td>23/02/2018 15:01:35</td>
                          <td>
                              <button type="button" class="btn btn-icon btn-success mr-1"><i class="ft-edit"></i></button>
                              <button type="button" class="btn btn-icon btn-success mr-1"><i class="ft-delete"></i></button>
                            </td>
                          
                        </tr>

                        <tr>
                          <td>02</td>
                          <td>Job2</td>
                          <td>****</td>
                          <td>User</td>
                          <td>This experience was wonderful</td>
                          <td>22/02/2018 11:01:35</td>                          
                          <td>22/02/2018 11:01:35</td>
                          <td>
                              <button type="button" class="btn btn-icon btn-success mr-1"><i class="ft-edit"></i></button>
                              <button type="button" class="btn btn-icon btn-success mr-1"><i class="ft-delete"></i></button>
                            </td>
                          
                        </tr>

                      </tbody>

                        
                        <tfoot>
                        <tr>
                          <th>Sl.No</th>
                          <th>Job Name</th>
                          <th>Ratings</th>
                          <th>Review By</th>
                          <th>Comments</th>
                          <th>Created At</th>
                          <th>Updated At</th>
                          <th>Action</th>
                          
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- // Basic form layout section end -->
      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent"
        target="_blank">PIXINVENT </a>, All rights reserved. </span>
      <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span>
    </p>
  </footer>
  <!-- BEGIN VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{URL::asset('public/app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>

  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN MODERN JS-->
  <script src="{{URL::asset('public/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/core/app.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/dropdowns/dropdowns.js')}}" type="text/javascript"></script>

  <!-- END MODERN JS-->
  <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{URL::asset('public/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
  type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>
</html>
@endsection