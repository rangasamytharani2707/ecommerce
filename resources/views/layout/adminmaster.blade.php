<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
  <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
  <meta name="author" content="PIXINVENT">
    <title>@yield('title')</title>
  <link rel="apple-touch-icon" href="{{URL::asset('public/app-assets/images/ico/apple-icon-120.png')}}">
  <!-- <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('public/app-assets/images/logo/logo_new.png')}}"> -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
  rel="stylesheet">
  <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/vendors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/core/menu/menu-types/vertical-menu-modern.css')}}">    
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/pickers/daterange/daterangepicker.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">
  
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/plugins/calendars/clndr.css')}}">
  

  <!-- END VENDOR CSS-->
  <!-- BEGIN MODERN CSS-->
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/app.css')}}">
  <!-- END MODERN CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/core/menu/menu-types/vertical-menu-modern.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/core/colors/palette-gradient.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/plugins/pickers/daterange/daterange.css')}}">
  
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/fonts/simple-line-icons/style.css')}}">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/assets/css/style.css')}}">
  <!-- END Custom CSS-->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/froala_editor.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/froala_style.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/code_view.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/draggable.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/colors.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/emoticons.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/image_manager.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/image.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/line_breaker.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/table.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/char_counter.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/video.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/fullscreen.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/file.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/quick_insert.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/help.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/third_party/spell_checker.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/app-assets/editor/css/plugins/special_characters.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css')}}">
  <link type="text/css" rel="stylesheet" href="{{URL::asset('public/assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>
  <link type="text/css" rel="stylesheet" href="{{URL::asset('public/assets/css/pages/sweet_alert.css')}}"/>
 <style>
     .checked {
         color: orange;
        }
       ul {
         list-style-type: none;
        }
        .link_clr{
          color: white;
        }
</style>
</head>

<body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
   
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
       @include('includes.header')
    </nav>
    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
       @include('includes.adminleftmenu')
    </div>
    @yield('content')
 </body>
<script type="text/javascript" src="{{URL::asset('public/assets/vendors/sweetalert/js/sweetalert2.min.js')}}"></script>